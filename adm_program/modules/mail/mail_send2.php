<?php
/******************************************************************************
 * Check email form and send email
 *
 * Copyright    : (c) 2004 - 2012 The Admidio Team
 * Homepage     : http://www.admidio.org
 * License      : GNU Public License 2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Parameters:
 *
 * usr_id  - Send email to this user
 * subject - set email subject independent of the form
 *
 *****************************************************************************/

require_once('../../system/common.php');
require_once('../../system/login_valid.php');
require_once('../../system/classes/email.php');
require_once('../../system/classes/table_roles.php');
require_once('../../libs/htmlawed/htmlawed.php');

// Initialize and check the parameters
$postRoleId = admFuncVariableIsValid($_POST, 'rol_id', 'numeric', 0);
$getUserId  = admFuncVariableIsValid($_GET, 'usr_id', 'numeric', 0);
$_POST['subject'] = admFuncVariableIsValid($_GET, 'subject', 'string', $_POST['subject']);

if ($gPreferences['enable_mail_module'] != 1) {
    // es duerfen oder koennen keine Mails ueber den Server verschickt werden
    $gMessage->show($gL10n->get('SYS_MODULE_DISABLED'));
}

if ($gCurrentUser->isWebmaster()) {
	$_POST['name'] = $gCurrentUser->getValue('FIRST_NAME'). ' '. $gCurrentUser->getValue('LAST_NAME'). ' ('.$g_organization.')';
} elseif ($gCurrentUser->getValue('usr_id') > 0) {
	$_POST['name'] = $gCurrentUser->getValue('FIRST_NAME'). ' '. $gCurrentUser->getValue('LAST_NAME');
	$_POST['mailfrom'] = $gCurrentUser->getValue('EMAIL');
}

$_SESSION['mail_request'] = $_REQUEST;


$to = array();


// alle Rollen auflisten,
// an die im eingeloggten Zustand Mails versendet werden duerfen
$result = $gDb->query('SELECT rol_name, rol_id, cat_name
          FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. '
         WHERE rol_valid   = 1
           AND rol_cat_id  = cat_id
           AND cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
         ORDER BY cat_sequence, rol_name ');
while ($row = $gDb->fetch_array($result)) {
    if($gValidLogin && $gCurrentUser->mailRole($row['rol_id'])) {
        $sql   = 'SELECT first_name.usd_value as first_name, last_name.usd_value as last_name,
                     email.usd_value as email, rol_name,usr_id,mem_begin,mem_end
                FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. ', '. TBL_MEMBERS. ', '. TBL_USERS. '
                JOIN '. TBL_USER_DATA. ' as email
                  ON email.usd_usr_id = usr_id
                 AND LENGTH(email.usd_value) > 0
                JOIN '.TBL_USER_FIELDS.' as field
                  ON field.usf_id = email.usd_usf_id
                 AND field.usf_type = \'EMAIL\'
                LEFT JOIN '. TBL_USER_DATA. ' as last_name
                  ON last_name.usd_usr_id = usr_id
                 AND last_name.usd_usf_id = '. $gProfileFields->getProperty('LAST_NAME', 'usf_id'). '
                LEFT JOIN '. TBL_USER_DATA. ' as first_name
                  ON first_name.usd_usr_id = usr_id
                 AND first_name.usd_usf_id = '. $gProfileFields->getProperty('FIRST_NAME', 'usf_id'). '
               WHERE rol_id      = '.$row['rol_id'].'
                 AND rol_cat_id  = cat_id
                 AND (  cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
                     OR cat_org_id IS NULL )
                 AND mem_rol_id  = rol_id
                 AND mem_usr_id  = usr_id
                 AND usr_valid   = 1
                ORDER BY `last_name`,`first_name`';

        $result2 = $gDb->query($sql);
        while ($user = $gDb->fetch_array($result2)) {
            if (isset($_POST['user_id_'.$row['rol_id'].'_'.$user['usr_id']]) && !array_key_exists($user['usr_id'], $to)) {
                $to[$user['usr_id']] = $user;
            }
        }
    }
}

// aktuelle Seite im NaviObjekt speichern. Dann kann in der Vorgaengerseite geprueft werden, ob das
// Formular mit den in der Session gespeicherten Werten ausgefuellt werden soll...
$_SESSION['navigation']->addUrl(CURRENT_URL);

// Falls Attachmentgroesse die max_post_size aus der php.ini uebertrifft, ist $_POST komplett leer.
// Deswegen muss dies ueberprueft werden...
if (empty($_POST))
{
    $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
}

//Erst mal ein neues Emailobjekt erstellen...
$email = new Email();

// und ein Dummy Rollenobjekt dazu
$role = new TableRoles($gDb);

//Nun der Mail die Absenderangaben,den Betreff und das Attachment hinzufuegen...
if(strlen($_POST['name']) == 0)
{
    $gMessage->show($gL10n->get('SYS_FIELD_EMPTY', $gL10n->get('SYS_NAME')));
}

//Absenderangaben checken falls der User eingeloggt ist, damit ein paar schlaue User nicht einfach die Felder aendern koennen...
if ( $gValidLogin
&& (
  !$gCurrentUser->isWebmaster() &&
  (  $_POST['mailfrom'] != $gCurrentUser->getValue('EMAIL')
   || $_POST['name'] != $gCurrentUser->getValue('FIRST_NAME').' '.$gCurrentUser->getValue('LAST_NAME')) ) )
{
    $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
}

//Absenderangaben setzen
if ($email->setSender($_POST['mailfrom'],$_POST['name']))
{
	//Betreff setzen
	if ($email->setSubject($_POST['subject']))
	{
        //Pruefen ob moeglicher Weise ein Attachment vorliegt
        if (isset($_FILES['userfile']))
        {
            //noch mal schnell pruefen ob der User wirklich eingelogt ist...
            if (!$gValidLogin)
            {
                $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
            }
            $attachment_size = 0;
            // Nun jedes Attachment
            for($act_attachment_nr = 0; isset($_FILES['userfile']['name'][$act_attachment_nr]) == true; $act_attachment_nr++)
            {
                //Pruefen ob ein Fehler beim Upload vorliegt
                if (($_FILES['userfile']['error'][$act_attachment_nr] != 0) &&  ($_FILES['userfile']['error'][$act_attachment_nr] != 4))
                {
                    $gMessage->show($gL10n->get('MAI_ATTACHMENT_TO_LARGE'));
                }
                //Wenn ein Attachment vorliegt dieses der Mail hinzufuegen
                if ($_FILES['userfile']['error'][$act_attachment_nr] == 0)
                {
                    // pruefen, ob die Anhanggroesse groesser als die zulaessige Groesse ist
                    $attachment_size = $attachment_size + $_FILES['userfile']['size'][$act_attachment_nr];
                    if($attachment_size > $email->getMaxAttachementSize("b"))
                    {
                        $gMessage->show($gL10n->get('MAI_ATTACHMENT_TO_LARGE'));
                    }

                    if (strlen($_FILES['userfile']['type'][$act_attachment_nr]) > 0)
                    {
                        $email->addAttachment($_FILES['userfile']['tmp_name'][$act_attachment_nr], $_FILES['userfile']['name'][$act_attachment_nr], $_FILES['userfile']['type'][$act_attachment_nr]);
                    }
                    // Falls kein ContentType vom Browser uebertragen wird,
                    // setzt die MailKlasse automatisch "application/octet-stream" als FileTyp
                    else
                    {
                        $email->addAttachment($_FILES['userfile']['tmp_name'][$act_attachment_nr], $_FILES['userfile']['name'][$act_attachment_nr]);
                    }
                }
            }
        }
    }
    else
    {
        $gMessage->show($gL10n->get('SYS_FIELD_EMPTY', $gL10n->get('MAI_SUBJECT')));
    }
}
else
{
    $gMessage->show($gL10n->get('SYS_EMAIL_INVALID', $gL10n->get('SYS_EMAIL')));
}

//Nun die Empfaenger zusammensuchen und an das Mailobjekt uebergeben
if (count($to)) {
    foreach ($to as $user) {
        $email->addBlindCopy($user['email'], $user['first_name'].' '.$user['last_name']);
    }
} else {
    // Falls in der Rolle kein User mit gueltiger Mailadresse oder die Rolle gar nicht in der Orga
    // existiert, muss zumindest eine brauchbare Fehlermeldung präsentiert werden...
    $gMessage->show($gL10n->get('MAI_ROLE_NO_EMAILS'));
}

if (isset($_POST['receiver_info']) && $_POST['receiver_info']) {
  $email->receiverInfo();
}

// Falls eine Kopie benoetigt wird, das entsprechende Flag im Mailobjekt setzen
if (isset($_POST['carbon_copy']) && $_POST['carbon_copy'] == true)
{
    $email->setCopyToSenderFlag();

    //Falls der User eingeloggt ist, werden die Empfaenger der Mail in der Kopie aufgelistet
    if ($gValidLogin)
    {
        $email->setListRecipientsFlag();
    }
}

// prepare body of email with note of sender and homepage
$email->setSenderInText($_POST['name'], $_POST['mailfrom'], '');

// make html in mail body secure and commit mail body to mail object
$email->setText(htmLawed(stripslashes($_POST['mail_body'])));

//Nun kann die Mail endgueltig versendet werden...
if ($email->sendEmail())
{
    // Der CaptchaCode wird bei erfolgreichem Mailversand aus der Session geloescht
    if (isset($_SESSION['captchacode']))
    {
        unset($_SESSION['captchacode']);
    }

    // Bei erfolgreichem Versenden wird aus dem NaviObjekt die am Anfang hinzugefuegte URL wieder geloescht...
    $_SESSION['navigation']->deleteLastUrl();
    // dann auch noch die mail.php entfernen
    $_SESSION['navigation']->deleteLastUrl();

    // Meldung ueber erfolgreichen Versand und danach weiterleiten
    if($_SESSION['navigation']->count() > 0)
    {
        $gMessage->setForwardUrl($_SESSION['navigation']->getUrl());
    }
    else
    {
        $gMessage->setForwardUrl($gHomepage);
    }


    $names = array();
    foreach ($to as $user) {
        $names[] = $user['first_name'].' '.$user['last_name'];
    }

    $gMessage->show($gL10n->get('SYS_EMAIL_SEND', implode(', ', $names)));

}
else
{
    if ($role->getValue('rol_id') > 0)
    {
        $gMessage->show($gL10n->get('SYS_EMAIL_NOT_SEND', $gL10n->get('MAI_TO_ROLE', $role->getValue('rol_name'))));
    }
    else
    {
        $gMessage->show($gL10n->get('SYS_EMAIL_NOT_SEND', $_POST['mailto']));
    }
}
