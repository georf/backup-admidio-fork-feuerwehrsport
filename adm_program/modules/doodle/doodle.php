<?php

require_once('../../system/common.php');
require_once('../../system/login_valid.php');
require_once('../../system/classes/email.php');
require_once('../../system/classes/form_elements.php');

if($gValidLogin == true && $gPreferences['mail_html_registered_users'] == 1)
{
	// create an object of ckeditor and replace textarea-element
	require_once('../../system/classes/ckeditor_special.php');
	$ckEditor = new CKEditorSpecial();
}

$formerMembers = 0;

// Initialize and check the parameters
$getRoleId      = admFuncVariableIsValid($_GET, 'rol_id', 'numeric', 0);
$getUserId      = admFuncVariableIsValid($_GET, 'usr_id', 'numeric', 0);
$getRoleName    = admFuncVariableIsValid($_GET, 'role_name', 'string', '');
$getCategory    = admFuncVariableIsValid($_GET, 'cat', 'string', '');
$getSubject     = admFuncVariableIsValid($_GET, 'subject', 'string', '');
$getBody        = admFuncVariableIsValid($_GET, 'body', 'string', '');
$getCarbonCopy  = admFuncVariableIsValid($_GET, 'carbon_copy', 'boolean', 1);
$getShowMembers = admFuncVariableIsValid($_GET, 'show_members', 'numeric', 0);

// Falls das Catpcha in den Orgaeinstellungen aktiviert wurde und die Ausgabe als
// Rechenaufgabe eingestellt wurde, muss die Klasse für nicht eigeloggte Benutzer geladen werden
if (!$gValidLogin && $gPreferences['enable_mail_captcha'] == 1 && $gPreferences['captcha_type']=='calc')
{
	require_once('../../system/classes/captcha.php');
}

// Pruefungen, ob die Seite regulaer aufgerufen wurde
if ($gPreferences['enable_mail_module'] != 1)
{
    // es duerfen oder koennen keine Mails ueber den Server verschickt werden
    $gMessage->show($gL10n->get('SYS_MODULE_DISABLED'));
}


if ($gValidLogin && strlen($gCurrentUser->getValue('EMAIL')) == 0)
{
    // der eingeloggte Benutzer hat in seinem Profil keine Mailadresse hinterlegt,
    // die als Absender genutzt werden kann...
    $gMessage->show($gL10n->get('SYS_CURRENT_USER_NO_EMAIL', '<a href="'.$g_root_path.'/adm_program/modules/profile/profile.php">', '</a>'));
}

//Falls ein Rollenname uebergeben wurde muss auch der Kategoriename uebergeben werden und umgekehrt...
if ( (strlen($getRoleName)  > 0 && strlen($getCategory) == 0)
||   (strlen($getRoleName) == 0 && strlen($getCategory)  > 0) )
{
    $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
}


if ($getUserId > 0)
{
    // Falls eine Usr_id uebergeben wurde, muss geprueft werden ob der User ueberhaupt
    // auf diese zugreifen darf oder ob die UsrId ueberhaupt eine gueltige Mailadresse hat...
    if (!$gValidLogin)
    {
        //in ausgeloggtem Zustand duerfen nie direkt usr_ids uebergeben werden...
        $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
    }

    //usr_id wurde uebergeben, dann Kontaktdaten des Users aus der DB fischen
    $user = new User($gDb, $gProfileFields, $getUserId);

    // darf auf die User-Id zugegriffen werden
    if((  $gCurrentUser->editUsers() == false
       && isMember($user->getValue('usr_id')) == false)
    || strlen($user->getValue('usr_id')) == 0 )
    {
        $gMessage->show($gL10n->get('SYS_USER_ID_NOT_FOUND'));
    }

    // besitzt der User eine gueltige E-Mail-Adresse
    if (!strValidCharacters($user->getValue('EMAIL'), 'email'))
    {
        $gMessage->show($gL10n->get('SYS_USER_NO_EMAIL', $user->getValue('FIRST_NAME').' '.$user->getValue('LAST_NAME')));
    }

    $userEmail = $user->getValue('EMAIL');
}
elseif ($getRoleId > 0 || (strlen($getRoleName) > 0 && strlen($getCategory) > 0))
{
    // wird eine bestimmte Rolle aufgerufen, dann pruefen, ob die Rechte dazu vorhanden sind

    if($getRoleId > 0)
    {
        $sqlConditions = ' AND rol_id = '.$getRoleId;
    }
    else
    {
        $sqlConditions = ' AND UPPER(rol_name) = UPPER(\''.$getRoleName.'\')
                           AND UPPER(cat_name) = UPPER(\''.$getCategory.'\')';
    }

    $sql = 'SELECT rol_mail_this_role, rol_name, rol_id,
                   (SELECT COUNT(1)
                      FROM '.TBL_MEMBERS.'
                     WHERE mem_rol_id = rol_id
                       AND (  mem_begin > \''.DATE_NOW.'\'
                           OR mem_end   < \''.DATE_NOW.'\')) as former
              FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. '
             WHERE rol_cat_id    = cat_id
               AND (  cat_org_id = '. $gCurrentOrganization->getValue('org_id').'
                   OR cat_org_id IS NULL)'.
                   $sqlConditions;
    $result = $gDb->query($sql);
    $row    = $gDb->fetch_array($result);

    // Ausgeloggte duerfen nur an Rollen mit dem Flag "alle Besucher der Seite" Mails schreiben
    // Eingeloggte duerfen nur an Rollen Mails schreiben, zu denen sie berechtigt sind
    // Rollen muessen zur aktuellen Organisation gehoeren
    if(($gValidLogin == false && $row['rol_mail_this_role'] != 3)
    || ($gValidLogin == true  && $gCurrentUser->mailRole($row['rol_id']) == false)
    || $row['rol_id']  == null)
    {
        $gMessage->show($gL10n->get('SYS_INVALID_PAGE_VIEW'));
    }

    $rollenName = $row['rol_name'];
    $rollenID   = $getRoleId;
    $formerMembers = $row['former'];
}

// Wenn die letzte URL in der Zuruecknavigation die des Scriptes mail_send.php ist,
// dann soll das Formular gefuellt werden mit den Werten aus der Session
if (strpos($_SESSION['navigation']->getUrl(),'mail_send.php') > 0 && isset($_SESSION['mail_request']))
{
    // Das Formular wurde also schon einmal ausgefüllt,
    // da der User hier wieder gelandet ist nach der Mailversand-Seite
    $form_values = strStripSlashesDeep($_SESSION['mail_request']);
    unset($_SESSION['mail_request']);

    $_SESSION['navigation']->deleteLastUrl();
}
else
{
    $form_values['name']         = '';
    $form_values['mailfrom']     = '';
    $form_values['subject']      = $getSubject;
    $form_values['mail_body']    = $getBody;
    $form_values['rol_id']       = '';
    $form_values['carbon_copy']  = $getCarbonCopy;
    $form_values['show_members'] = $getShowMembers;
}

// Seiten fuer Zuruecknavigation merken
if($getUserId == 0 && $getRoleId == 0)
{
    $_SESSION['navigation']->clear();
}
$_SESSION['navigation']->addUrl(CURRENT_URL);

// Focus auf das erste Eingabefeld setzen
if ($getUserId == 0 && $getRoleId == 0 && strlen($getRoleName)  == 0)
{
    $focusField = 'rol_id';
}
else if($gCurrentUser->getValue('usr_id') == 0)
{
    $focusField = 'name';
}
else
{
    $focusField = 'subject';
}

// Html-Kopf ausgeben
if (strlen($getSubject) > 0)
{
    $gLayout['title'] = $getSubject;
}
else
{
    $gLayout['title'] = $gL10n->get('MAI_SEND_EMAIL');
}


// alle Rollen auflisten,
// an die im eingeloggten Zustand Mails versendet werden duerfen
$sql = 'SELECT rol_id, rol_name
          FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. '
         WHERE rol_valid   = 1
           AND rol_cat_id  = cat_id
           AND cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
         ORDER BY cat_sequence, rol_name ';

$result = $gDb->query($sql);
$roles = array();

while ($row = $gDb->fetch_array($result))
{

    $sql   = 'SELECT first_name.usd_value as first_name, last_name.usd_value as last_name,
                     email.usd_value as email, rol_name,usr_id,mem_begin,mem_end
                FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. ', '. TBL_MEMBERS. ', '. TBL_USERS. '
                JOIN '. TBL_USER_DATA. ' as email
                  ON email.usd_usr_id = usr_id
                 AND LENGTH(email.usd_value) > 0
                JOIN '.TBL_USER_FIELDS.' as field
                  ON field.usf_id = email.usd_usf_id
                 AND field.usf_type = \'EMAIL\'
                LEFT JOIN '. TBL_USER_DATA. ' as last_name
                  ON last_name.usd_usr_id = usr_id
                 AND last_name.usd_usf_id = '. $gProfileFields->getProperty('LAST_NAME', 'usf_id'). '
                LEFT JOIN '. TBL_USER_DATA. ' as first_name
                  ON first_name.usd_usr_id = usr_id
                 AND first_name.usd_usf_id = '. $gProfileFields->getProperty('FIRST_NAME', 'usf_id'). '
               WHERE rol_id      = '.$row['rol_id'].'
                 AND rol_cat_id  = cat_id
                 AND (  cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
                     OR cat_org_id IS NULL )
                 AND mem_rol_id  = rol_id
                 AND mem_usr_id  = usr_id
                 AND usr_valid   = 1
                ORDER BY `last_name`,`first_name`';

    $result2 = $gDb->query($sql);
    $users = array();
    while ($user = $gDb->fetch_array($result2))
    {
        $users[] = array(
            'id' => $user['usr_id'],
            'begin' => $user['mem_begin'],
            'end' => $user['mem_end'],
            'name' => $user['first_name'].' '.$user['last_name']
        );
    }
    $roles[] = array(
        'id' => $row['rol_id'],
        'name' => $row['rol_name'],
        'users' => $users
    );
}

$gLayout['header'] =  "
<script type=\"text/javascript\"><!--

// neue Zeile mit Button zum Hinzufuegen von Dateipfaden einblenden
    function addAttachment()
    {
        new_attachment = document.createElement('input');
        $(new_attachment).attr('type', 'file');
        $(new_attachment).attr('name', 'userfile[]');
        $(new_attachment).css('display', 'block');
        $(new_attachment).css('width', '90%');
        $(new_attachment).css('margin-bottom', '5px');
        $(new_attachment).hide();
        $('#add_attachment').before(new_attachment);
        $(new_attachment).show('slow');
    }


$(function() {

    // uncheck all members
    $('.rol_member').attr('checked', false);
    $('.rol').attr('checked', false);


    $('.rol_member').change(function() {
        var rol_id;
        var checked = $(this).is(':checked');

        if (!checked) {
            // uncheck current role
            rol_id = $(this).attr('id').replace(/^user_id_([0-9]+)_[0-9]+$/, '$1');
            $('#rol_id_' + rol_id).attr('checked', false);
        }


        $('.user_id_' + $(this).val()).attr('checked', checked);
    });

    $('.rol').change(function() {
        var checked = $(this).is(':checked');

        $('.rol_id_' + $(this).val()).each(function() {
            if (checked && $(this).hasClass('user_active')) {
                $(this).attr('checked', checked).change();
            } else if(!checked) {
                $(this).attr('checked', checked).change();
            }
        });
    });

});

//--></script>";

require(SERVER_PATH. '/adm_program/system/overall_header.php');
echo '
<form action="'.$g_root_path.'/adm_program/modules/doodle/doodle-generate.php?';
    // usr_id wird mit GET uebergeben,
    // da keine E-Mail-Adresse von mail_send angenommen werden soll
    if($getUserId > 0)
    {
        echo 'usr_id='.$getUserId.'&';
    }
	if (strlen($getSubject) > 0)
	{
		echo 'subject='.$getSubject.'&';
	}
    echo '" method="post" enctype="multipart/form-data">

    <div class="formLayout" id="write_mail_form">
        <div class="formHead">Doodle-E-Mail generieren</div>
        <div class="formBody">
			<div class="groupBox" id="admMailContactDetails">
				<div class="groupBoxHeadline" id="admMailContactDetailsHead">
					<a class="iconShowHide" href="javascript:showHideBlock(\'admMailContactDetailsBody\', \''.$gL10n->get('SYS_FADE_IN').'\', \''.$gL10n->get('SYS_HIDE').'\')"><img
					id="admMailContactDetailsBodyImage" src="'. THEME_PATH. '/icons/triangle_open.gif" alt="'.$gL10n->get('SYS_HIDE').'" title="'.$gL10n->get('SYS_HIDE').'" /></a>'.$gL10n->get('SYS_CONTACT_DETAILS').'
				</div>

				<div class="groupBoxBody" id="admMailContactDetailsBody">
					<ul class="formFieldList">
						<li>
<p>'.$gL10n->get('SYS_TO').':</p>
<ul>';

// alle Rollen auflisten,
// an die im eingeloggten Zustand Mails versendet werden duerfen
$sql = 'SELECT rol_name, rol_id, cat_name
          FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. '
         WHERE rol_valid   = 1
           AND rol_cat_id  = cat_id
           AND cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
         ORDER BY cat_sequence, rol_name ';

$result = $gDb->query($sql);
$act_category = '';

while ($row = $gDb->fetch_array($result)) {
    if($gValidLogin && $gCurrentUser->mailRole($row['rol_id'])) {
        echo '<li>';
        echo '<input type="checkbox" name="rol_id_'.$row['rol_id'].'" id="rol_id_'.$row['rol_id'].'" class="rol" value="'.$row['rol_id'].'"/>';
        echo '<label for="rol_id_'.$row['rol_id'].'">'.$row['rol_name'].'</label>';
        echo '<a class="iconShowHide" href="javascript:showHideBlock(\'members_'.$row['rol_id'].'\', \''.$gL10n->get('SYS_FADE_IN').'\', \''.$gL10n->get('SYS_HIDE').'\')"><img
					id="members_'.$row['rol_id'].'Image" src="'. THEME_PATH. '/icons/triangle_close.gif" alt="'.$gL10n->get('SYS_HIDE').'" title="'.$gL10n->get('SYS_HIDE').'" /></a>';
        echo '</li>';

        $sql   = 'SELECT first_name.usd_value as first_name, last_name.usd_value as last_name,
                     email.usd_value as email, rol_name,usr_id,mem_begin,mem_end
                FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. ', '. TBL_MEMBERS. ', '. TBL_USERS. '
                JOIN '. TBL_USER_DATA. ' as email
                  ON email.usd_usr_id = usr_id
                 AND LENGTH(email.usd_value) > 0
                JOIN '.TBL_USER_FIELDS.' as field
                  ON field.usf_id = email.usd_usf_id
                 AND field.usf_type = \'EMAIL\'
                LEFT JOIN '. TBL_USER_DATA. ' as last_name
                  ON last_name.usd_usr_id = usr_id
                 AND last_name.usd_usf_id = '. $gProfileFields->getProperty('LAST_NAME', 'usf_id'). '
                LEFT JOIN '. TBL_USER_DATA. ' as first_name
                  ON first_name.usd_usr_id = usr_id
                 AND first_name.usd_usf_id = '. $gProfileFields->getProperty('FIRST_NAME', 'usf_id'). '
               WHERE rol_id      = '.$row['rol_id'].'
                 AND rol_cat_id  = cat_id
                 AND (  cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
                     OR cat_org_id IS NULL )
                 AND mem_rol_id  = rol_id
                 AND mem_usr_id  = usr_id
                 AND usr_valid   = 1
                ORDER BY `last_name`,`first_name`';

        echo '<ul id="members_'.$row['rol_id'].'" style="display:none">';

        $result2 = $gDb->query($sql);
        $users = array();
        while ($user = $gDb->fetch_array($result2)) {
            $active = ($user['mem_begin'] <= DATE_NOW && $user['mem_end'] > DATE_NOW);

            echo '<li>';
            echo '<input value="'.$user['usr_id'].'" class="user_id_'.$user['usr_id'].' rol_id_'.$row['rol_id'].' rol_member ';

            if ($active) echo 'user_active';

            echo '" type="checkbox" name="user_id_'.$row['rol_id'].'_'.$user['usr_id'].'" id="user_id_'.$row['rol_id'].'_'.$user['usr_id'].'"/>';
            echo '<label ';
            if (!$active) echo ' style="background:#E5E5E5" ';
            echo ' for="user_id_'.$row['rol_id'].'_'.$user['usr_id'].'">'.$user['last_name'].', '.$user['first_name'].'</label>';
            echo '</li>';
        }

        echo '</ul>';

    }
}
echo'
</ul>
</li>';



						echo '
					</ul>
				</div>
                <div id="members_select" style="display:none;"></div>
			</div>';

            echo '<div class="formSubmit">
                <button id="btnSend" type="submit"><img src="'. THEME_PATH. '/icons/email.png" alt="'.$gL10n->get('SYS_SEND').'" />&nbsp;'.$gL10n->get('SYS_SEND').'</button>
            </div>
        </div>
    </div>
</form>';

if($getUserId > 0 || $getRoleId > 0)
{
    echo '
    <ul class="iconTextLinkList">
        <li>
            <span class="iconTextLink">
                <a href="'.$g_root_path.'/adm_program/system/back.php"><img
                src="'. THEME_PATH. '/icons/back.png" alt="'.$gL10n->get('SYS_BACK').'" /></a>
                <a href="'.$g_root_path.'/adm_program/system/back.php">'.$gL10n->get('SYS_BACK').'</a>
            </span>
        </li>
    </ul>';
}

require(SERVER_PATH. '/adm_program/system/overall_footer.php');

?>
