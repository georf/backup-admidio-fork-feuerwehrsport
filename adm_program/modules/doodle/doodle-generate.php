<?php
/******************************************************************************
 * Check email form and send email
 *
 * Copyright    : (c) 2004 - 2012 The Admidio Team
 * Homepage     : http://www.admidio.org
 * License      : GNU Public License 2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Parameters:
 *
 * usr_id  - Send email to this user
 * subject - set email subject independent of the form
 *
 *****************************************************************************/

require_once('../../system/common.php');
require_once('../../system/login_valid.php');
require_once('../../system/classes/email.php');
require_once('../../system/classes/table_roles.php');
require_once('../../libs/htmlawed/htmlawed.php');

// Initialize and check the parameters
$postRoleId = admFuncVariableIsValid($_POST, 'rol_id', 'numeric', 0);
$getUserId  = admFuncVariableIsValid($_GET, 'usr_id', 'numeric', 0);
$_POST['subject'] = admFuncVariableIsValid($_GET, 'subject', 'string', $_POST['subject']);

if ($gPreferences['enable_mail_module'] != 1) {
    // es duerfen oder koennen keine Mails ueber den Server verschickt werden
    $gMessage->show($gL10n->get('SYS_MODULE_DISABLED'));
}

if ($gCurrentUser->isWebmaster()) {
	$_POST['name'] = $gCurrentUser->getValue('FIRST_NAME'). ' '. $gCurrentUser->getValue('LAST_NAME'). ' ('.$g_organization.')';
} elseif ($gCurrentUser->getValue('usr_id') > 0) {
	$_POST['name'] = $gCurrentUser->getValue('FIRST_NAME'). ' '. $gCurrentUser->getValue('LAST_NAME');
	$_POST['mailfrom'] = $gCurrentUser->getValue('EMAIL');
}

$_SESSION['mail_request'] = $_REQUEST;


$to = array();


// alle Rollen auflisten,
// an die im eingeloggten Zustand Mails versendet werden duerfen
$result = $gDb->query('SELECT rol_name, rol_id, cat_name
          FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. '
         WHERE rol_valid   = 1
           AND rol_cat_id  = cat_id
           AND cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
         ORDER BY cat_sequence, rol_name ');
while ($row = $gDb->fetch_array($result)) {
    if($gValidLogin && $gCurrentUser->mailRole($row['rol_id'])) {
        $sql   = 'SELECT first_name.usd_value as first_name, last_name.usd_value as last_name,
                     email.usd_value as email, rol_name,usr_id,mem_begin,mem_end
                FROM '. TBL_ROLES. ', '. TBL_CATEGORIES. ', '. TBL_MEMBERS. ', '. TBL_USERS. '
                JOIN '. TBL_USER_DATA. ' as email
                  ON email.usd_usr_id = usr_id
                 AND LENGTH(email.usd_value) > 0
                JOIN '.TBL_USER_FIELDS.' as field
                  ON field.usf_id = email.usd_usf_id
                 AND field.usf_type = \'EMAIL\'
                LEFT JOIN '. TBL_USER_DATA. ' as last_name
                  ON last_name.usd_usr_id = usr_id
                 AND last_name.usd_usf_id = '. $gProfileFields->getProperty('LAST_NAME', 'usf_id'). '
                LEFT JOIN '. TBL_USER_DATA. ' as first_name
                  ON first_name.usd_usr_id = usr_id
                 AND first_name.usd_usf_id = '. $gProfileFields->getProperty('FIRST_NAME', 'usf_id'). '
               WHERE rol_id      = '.$row['rol_id'].'
                 AND rol_cat_id  = cat_id
                 AND (  cat_org_id  = '. $gCurrentOrganization->getValue('org_id'). '
                     OR cat_org_id IS NULL )
                 AND mem_rol_id  = rol_id
                 AND mem_usr_id  = usr_id
                 AND usr_valid   = 1
                ORDER BY `last_name`,`first_name`';

        $result2 = $gDb->query($sql);
        while ($user = $gDb->fetch_array($result2)) {
            if (isset($_POST['user_id_'.$row['rol_id'].'_'.$user['usr_id']]) && !array_key_exists($user['usr_id'], $to)) {
                $to[$user['usr_id']] = $user;
            }
        }
    }
}

if (count($to)) {
    $names = array();

    foreach ($to as $user) {
        $names[] = $user['first_name'].' '.$user['last_name'].'<'.$user['email'].'>';
    }

    $gMessage->show(htmlspecialchars(implode(", ", $names)));
}
