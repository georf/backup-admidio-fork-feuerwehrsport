<?php
include __DIR__."/PHPMailer/PHPMailerAutoload.php";

class Email {

private $text;              // plain text of email

public function __construct() {
    global $gPreferences;

    $this->addresses = array();
    $this->names = array();
    $this->copyToSender        = false;
    $this->listRecipients      = false;
    $this->text                = '';
    $this->receiverInfo = false;

    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = PHPMAILER_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = PHPMAILER_USER;
    $mail->Password = PHPMAILER_PASS;
    $mail->SMTPSecure = 'tls';
    $mail->isHTML(false);
    $mail->CharSet = 'UTF-8';

    $this->phpMailer = $mail;
    $this->phpMailer->setFrom(PHPMAILER_USER, PHPMAILER_FROM_NAME);

    $mail2 = new PHPMailer;
    $mail2->isSMTP();
    $mail2->Host = PHPMAILER_HOST;
    $mail2->SMTPAuth = true;
    $mail2->Username = PHPMAILER_USER;
    $mail2->Password = PHPMAILER_PASS;
    $mail2->SMTPSecure = 'tls';
    $mail2->isHTML(false);
    $mail2->CharSet = 'UTF-8';

    $this->phpMailer2 = $mail2;
    $this->phpMailer2->setFrom(PHPMAILER_USER, PHPMAILER_FROM_NAME);

}

public function receiverInfo() {
    $this->receiverInfo = true;
}

public function addAttachment($tempFilename, $originalFilename = '', $fileType='application/octet-stream' ,
					   $fileDisposition = 'attachment',$fileId = '') {
    $this->phpMailer->addAttachment($tempFilename, $originalFilename);
    $this->phpMailer2->addAttachment($tempFilename, $originalFilename);
    return true;
}

public function addBlindCopy($address, $name='') {
    $this->phpMailer->addBCC($address, $name);
    $this->addresses[] = $name." <".$address.">";
    $this->names[] = $name;
    return true;
}

public function addCopy($address, $name='') {
    $this->phpMailer->addCC($address, $name);
    $this->addresses[] = $name." <".$address.">";
    return true;
}

public function addRecipient($address, $name='') {
    $this->phpMailer->addAddress($address, $name);
    $this->addresses[] = $name." <".$address.">";
    return true;
}


// Methode gibt die maximale Groesse der Anhaenge zurueck
// size_unit : 'b' = byte; 'kb' = kilobyte; 'mb' = megabyte
public static function getMaxAttachementSize($size_unit = 'kb')
{
    global $gPreferences;

    if(round(admFuncMaxUploadSize()/pow(1024, 1), 1) < $gPreferences['max_email_attachment_size'])
    {
        $attachment_size = round(admFuncMaxUploadSize()/pow(1024, 1), 2);
    }
    else
    {
        $attachment_size = $gPreferences['max_email_attachment_size'];
    }

    if($size_unit == 'mb')
    {
        $attachment_size = $attachment_size / 1024;
    }
    elseif($size_unit == 'b')
    {
        $attachment_size = $attachment_size * 1024;
    }
    return round($attachment_size, 2);
}

public function setSender($address, $name='') {
    $this->phpMailer->addReplyTo($address, $name);
    $this->phpMailer2->addAddress($address, $name);
    return true;
}

// write a short text with sender informations in text of email
public function setSenderInText($senderName, $senderEmail, $roleName)
{
    global $gL10n, $gValidLogin, $gCurrentOrganization;

    $senderCode = $senderName.' ('.$senderEmail.')';

    if(strlen($roleName) > 0) {
        $senderText = $gL10n->get('MAI_EMAIL_SEND_TO_ROLE', $senderCode, $gCurrentOrganization->getValue('org_homepage'), $roleName);
    } else {
        $senderText = $gL10n->get('MAI_EMAIL_SEND_TO_USER', $senderCode, $gCurrentOrganization->getValue('org_homepage'));
    }

    if($gValidLogin == false) {
        $senderText = $senderText."\r\n".$gL10n->get('MAI_SENDER_NOT_LOGGED_IN');
    }

    $senderText = $senderText."\r\n".
    '*****************************************************************************************************************************'.
    "\r\n"."\r\n";

    $this->text = $this->text.$senderText;
    return true;
}

public function setSubject($subject) {
    $this->phpMailer->Subject = $subject;
    $this->phpMailer2->Subject = $subject;
    return true;
}

public function setText($text) {
    $text = str_replace('\r\n', '\n', $text);
    $text = str_replace('\r', '\n', $text);

    $this->text = $this->text.strip_tags($text);
    return true;
}

public function setCopyToSenderFlag() {
    $this->copyToSender = true;
    return true;
}

public function setListRecipientsFlag() {
    $this->listRecipients = true;
    return true;
}

public function sendDataAsHtml() {
    return true;
}

public function sendEmail() {
    global $gPreferences, $gL10n;
    $text = $this->text;
    if ($this->receiverInfo) {
        $text.="\r\n******************************************************************";
        $text.="\r\nDiese E-Mail ging an folgende Personen: ";
        $text.="\r\n".implode(", ", $this->names);
        $text.="\r\n";
    }

    $this->phpMailer->Body = $text;

    if (!$this->phpMailer->send()) {
      return false;
    }

    // Eventuell noch eine Kopie an den Absender verschicken:
    if ($this->copyToSender) {
        $copyHeader = '*****************************************************************************************************************************'.
                      "\r\n"."\r\n";
        $copyHeader = $gL10n->get('MAI_COPY_OF_YOUR_EMAIL').':'."\r\n".$copyHeader;

        if ($this->listRecipients) {
            $copyHeader = implode(', ', $this->addresses)."\r\n".$copyHeader;
            $copyHeader = $gL10n->get('MAI_MESSAGE_WENT_TO').':'."\r\n"."\r\n".$copyHeader;
        }

        $text = $copyHeader.$text;

        $this->phpMailer2->Body = $text;

        $this->phpMailer2->Subject = $gL10n->get('MAI_CARBON_COPY').': '. $this->phpMailer2->Subject;

        if (!$this->phpMailer2->send()) {
           // echo 'Message could not be sent.';
           // echo 'Mailer Error: ' . $this->phpMailer2->ErrorInfo;
           // exit;
          return false;
        }
    }
    return true;
}
}
?>
